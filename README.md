# README #
Covid Disease Ontology.<br>
Database 2 2021-2022 first group project.<br>
University of Padua, DEI department.

***
## Table of Contents
1. [General Info](#general-info)
2. [Description](#description)
3. [Source of Open Data](#opendata)

******************************
## General Info
### Group
* OwlParliament

### Members ###
* Emanuela Pozzer
* Riccardo Riva
* Riccardo Smerilli

*********************************
## Description
### Description of the project 
This project regards building an RDF database about the Covid Disease. <br>
It has been modelled the information about the new Cases and the new Deaths per Country, which are grouped in different world Regions.<br>
Furthermore, in the database there are the Vaccinations and the used Vaccine for each country. <br>
To improve the knowledge about Covid disease, the Fundings, Researches and relative Institutions are considered in our graph.


### What is in this repository 
* Image of the entities relations graph representing the database.
* The OWL file both in Turtle and XML format.
* The .csv files containing covid datasets.
* The python notebook for data ingestion and serialization.
* The .ttl files (one for each entity in the graph) containing the serializations.
* The queries files. Each file is a link that opens the query in graphDB.
* The project presentation.

***
## Source of Open Data
### Information about the source of the .csv files
Following .csv files are taken from: [https://covid19.who.int](https://covid19.who.int/info/).
* vaccination-metadata.csv
* vaccination-data.csv
* WHO-COVID-19-global-data.csv

Following .csv files are taken from: [https://publicmeds4covid.org](https://publicmeds4covid.org/data)
* fundings_10_17_2021.csv
* researches_10_17_2021.csv
* institutions_10_17_2021.csv

The .csv file "wikipedia-iso-country-codes.csv" is taken from: [https://github.com/](https://gist.github.com/radcliff/f09c0f88344a7fcef373)

